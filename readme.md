# Domain Driven Design Library

## Idea

We will reproduce a simple Library enviroment. Next we will documentate the required main concepts and functionalities to play with.

#### The Library

The library will have mainly `Books` that will be read by `Readers`. These `Readers` can lease the `Books` for a limited time.

`Books`are not infinite so we have a stock of them. We must check if a `Book` is currently available when a `Reader` leases a `Book`.

A `Book` will be identified with `ISBN` Value Object.

#### Value Objects
- ISBN
- Lease
- Date

## Future Implementations

For the `Readers`:
· Roles
· Stock notification of a Book. A Reader can subscribe to recive a notification when a Book comes available on stock.