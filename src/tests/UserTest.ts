import "mocha";
import { expect, assert } from 'chai';

import { User } from '../domain/models/User';
import { Uuid } from "../domain/value_objects/Uuid";

describe('User Tests', () => {
    
    it("Should be instaceOf User", () => {
        let myUser = new User("Paco", "Flores");

        assert.instanceOf(myUser, User, "Created User is an User object");
    });

    it("Get Uuid typeOf", () => {
        let myUser = new User("Pepito", "Flores");
        assert.instanceOf(myUser.getUuid(), Uuid, "Returned value is instanceOf Uuid");

    });
});