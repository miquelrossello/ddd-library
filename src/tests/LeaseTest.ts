import "mocha";
import { expect, assert } from "chai";

import { Lease } from "../domain/models/Lease";
import { Date } from "../domain/value_objects/Date";

describe("Lease Tests", () => {
    
    it("Shouldn't be able to set end date minor than start date", () => {
        expect(() => new Lease(new Date("2020-01-01"), new Date("2019-12-01"))).throw();
    });

    it("Should be a positive number when gets diff days between two dates", () => {
        let myLease = new Lease(new Date("2020-01-01"), new Date("2020-03-01"));
        
        let diffDays = myLease.diffDays() >= 0;

        expect(diffDays).to.be.true;
    });

});