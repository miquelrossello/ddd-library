import "mocha";
import { expect, assert } from "chai";

import { Date } from "../domain/value_objects/Date";
import moment = require("moment");

describe("Date Tests", () => {
    it ("Should create a date", () => {
        let date = new Date("2020-01-01");

        assert.instanceOf(date, Date, "Instance of Date");
    });

    it("Should throw error", () => {
        expect(() => {
            let date = new Date("01-01-2010");
        }).throw();
    });

    it("Should return a Moment object", () => {
        expect(new Date("2020-01-01").getMoment()).be.instanceOf(moment);
    });
});