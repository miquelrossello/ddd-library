export class ISBN {

    private isbn: string;

    public constructor(isbn: string) {
        this.setIsbn(isbn);
    }

    public getIsbn(): string {
        return this.isbn;
    }

    /**
     * Not following Official ISBN pattern.
     * Only checking if ISBN have a length of 13.
     * @param isbn Must be a 13 length string
     */
    public setIsbn(isbn: string) :void {
        if (!isbn) throw new Error('Invalid Argument');

        // Regex of ISBN Pattern
        let isbn_regex = new RegExp('^(?:ISBN(?:-1[03])?:?●)?(?=[0-9X]{10}$|(?=(?:[0-9]+[-●]){3})[-●0-9X]{13}$|97[89][0-9]{10}$|(?=(?:[0-9]+[-●]){4})[-●0-9]{17}$)(?:97[89][-●]?)?[0-9]{1,5}[-●]?[0-9]+[-●]?[0-9]+[-●]?[0-9X]$');
        if(!isbn_regex.test(isbn)) throw new Error('Invalid ISBN');
    
        this.isbn = isbn;
    }

}