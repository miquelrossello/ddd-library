import { v4 as uuid } from 'uuid'; 

export class Uuid {

    private uuid: string;

    constructor(uuid: string) {
        this.setUuid(uuid);
    }

    public static generate(): Uuid {
        return new Uuid(uuid());
    }

    private setUuid(uuid: string): void {
        this.uuid = uuid;
    }    
}