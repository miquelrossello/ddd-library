import { Moment } from "moment"
import moment = require("moment");

export class Date {

    private date: string;
    private dateFormat: string = "YYYY-MM-DD";

    /**
     * @param date Date in string format 
     * @param dateFormat Set a defualt dateFormat. 
     */
    constructor(date: string) {
        this.setDate(date, this.dateFormat);
    }

    public setDate(date: string, dateFormat: string): void {
        if (!date) throw new Error("Date is empty");

        // We specify true as third parameter to enable strict mode and check dateFormat strictly.
        let momentDate = moment(date, dateFormat, true);

        if (!momentDate.isValid()) throw new Error("Invalid Date");

        this.date = momentDate.format(this.dateFormat);
    }

    public getDate(): string {
        return this.date;
    }

    public getMoment(): Moment {
        return moment(this.date, this.dateFormat, true);
    }

}