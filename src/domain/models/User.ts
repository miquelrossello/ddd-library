import { Uuid } from "../value_objects/Uuid";

export class User {

    private uuid: Uuid;
    private fisrtName: string;
    private secondName: string;

    constructor(firstName: string, secondName?: string) {
        this.setUuid(Uuid.generate());
        this.setFirstName(firstName);

        if (secondName) {
            this.setSecondName(secondName);
        }
    }

    private setUuid(uuid: Uuid): void {
        if (!uuid) throw new Error("Can't set 'uuid' attribute from empty");

        this.uuid = uuid;
    }

    private setFirstName(firstName: string): void {
        if (firstName === "") throw new Error("Can't set 'firstName' from empty");

        this.fisrtName = firstName;
    }

    private setSecondName(secondName: string): void {
        if (secondName === "") throw new Error("Can't set 'secondName' from empty");

        this.secondName = secondName;
    }

    public getUuid(): Uuid {
        return this.uuid;
    }

    public getFirstName(): string {
        return this.fisrtName;
    }

    public getSecondName(): string {
        return this.secondName;
    }
}