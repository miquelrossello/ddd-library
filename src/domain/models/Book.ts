import { ISBN } from "../value_objects/ISBN";

export class Book {

    private isbn: ISBN;
    private title: String;

    /**
     * Constructs a Book.
     * @param title Required param to create a Book instance
     */
    constructor(isbn: ISBN, title: String) {
        if (title === "") throw new Error("Can't create a Book with empty Title attribute");

        this.setIsbn(isbn);
        this.setTitle(title);
    }

    /**
     * Return the title attribute
     * @returns The title as string
     */
    public getTitle(): String {
        return this.title;
    }

    /**
     * Checking only if the string is not null.
     * 
     * This probbably could be a ValueObject as we would like
     * to set a max length, for example.
     * 
     * @param title A not null string
     */
    public setTitle(title: String) {
        if (!title || title === "") throw new Error('Invalid Arugment');

        this.title = title;
    }

    public getIsbn(): ISBN {
        return this.isbn;
    }

    /**
     * Here we don't have to check anyting because we already checked in
     * the ValueObject ISBN object. If the ISBN isn't valid it will not
     * be created (or should not jeje).
     * @param isbn 
     */
    public setIsbn(isbn: ISBN): void {
        this.isbn = isbn;
    }

    /**
     * Returns if the Book is availabe
     * @returns True or False depending if is currently available
     */
    public isAvailable() {}
}
