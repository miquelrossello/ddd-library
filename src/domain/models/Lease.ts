import { Date } from "../value_objects/Date";


export class Lease {

    private initDate: Date;
    private endDate: Date;


    constructor(initDate: Date, endDate: Date) {
        this.setInitDate(initDate);
        this.setEndDate(endDate);
    }

    // On setters we could set logic as if we
    // should check a Lease with an init date greate
    // than now (Date).

    // NO DATES LOGIC FOR NOW

    public setInitDate(initDate: Date): void {
        this.initDate = initDate;
    }

    public setEndDate(endDate: Date): void {
        if (this.initDate.getMoment().isAfter(endDate.getDate())) throw new Error("End date minor than Init date");

        this.endDate = endDate;
    }

    public getInitDate(): Date {
        return this.initDate;
    }

    public getEndDate(): Date {
        return this.endDate;
    }

    /**
     * Returns the number of days between init and end dates
     */
    public diffDays(): number {
        return this.endDate.getMoment().diff(this.initDate.getDate(), 'days');
    }
}